<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;

class APICatalogController extends Controller
{
    public function index(){
        return response()->json(Movie::all());
    }

    public function show($id){
        return response()->json(Movie::findOrFail($id)); 
    }

    public function putRent($id){
        $m = Movie::findOrFail( $id );
        $m->rented = true; 
        $m->save();
        return response()->json(['error' => false, 'msg' => 'La película se ha marcado como alquilada']);
    }

    public function putReturn($id){
        $m = Movie::findOrFail( $id );
        $m->rented = false; 
        $m->save();
        return response()->json(['error' => false, 'msg' => 'La película se ha marcado como no alquilada']);
    }

    public function store(){
        return response()->json(['error'=> false,'msg'=> "Se creara una película"]);
    }

    public function update(Request $request,$id){
        $m = Movie::findOrFail($id);
        $m->fill($request->all());
        $m->save();
        return response()->json(['error' => false, 'msg' => 'La película se ha actualizado']);
    }

    public function destroy($id){
        $m = Movie::findOrFail($id);
        $m->delete();
        return response()->json(['error' => false, 'msg' => 'La pelicula se ha eliminado']);
    }
}
