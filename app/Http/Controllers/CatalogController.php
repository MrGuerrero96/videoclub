<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
#use Illuminate\Support\Facades\DB;
use App\Movie;

class CatalogController extends Controller
{
    public function getShow($id){
        $pelicula = Movie::findOrFail($id);
        return view('catalog.show', array('pelicula' => $pelicula)); 
    }

    public function getCreate(Request $request){
        return view('catalog.create'); 
    }

    public function getEdit($id, Request $request){
        $pelicula = Movie::findOrFail($id);
        return view('catalog.edit', array('pelicula'=>$pelicula)); 
    }

    public function getIndex(){
        $peliculas = Movie::all();
        #$peliculas = DB::table('movies')->get();
        return view('catalog.index',array('peliculas'=>$peliculas)); 
    }

    public function postCreate(Request $request){
        $pelicula = new Movie;
        $pelicula->title = $request->input('title');
        $pelicula->year = $request->input('año');
        $pelicula->director = $request->input('director');
        $pelicula->poster = $request->input('poster');
        $pelicula->synopsis = $request->input('synopsis');
        $pelicula->save();
        notify('La pelicula se ha creado correctamente')->type('success');
        return redirect()->action('CatalogController@getIndex');
    }
    public function putEdit(Request $request,$id){
        $pelicula = Movie::FindOrFail($id);
        $pelicula->title = $request->input('title');
        $pelicula->year = $request->input('año');
        $pelicula->director = $request->input('director');
        $pelicula->poster = $request->input('poster');
        $pelicula->synopsis = $request->input('synopsis');
        $pelicula->save();
        notify('La pelicula se ha modificado correctamente')->type('success');
        return redirect()->action('CatalogController@getShow',$id);
    }

    public function putRent($id){
        $pelicula = Movie::FindOrFail($id);
        $pelicula->rented = true;
        $pelicula->save();
        notify('La pelicula se ha rentado correctamente')->type('success');
        return redirect()->action('CatalogController@getShow',$id);
    }

    public function putReturn($id){
        $pelicula = Movie::FindOrFail($id);
        $pelicula->rented = false;
        $pelicula->save();
        notify('La pelicula se ha devuelto correctamente')->type('success');
        return redirect()->action('CatalogController@getShow',$id); 
    }

    public function deleteMovie($id){
        $pelicula = Movie::FindOrFail($id);
        $pelicula->delete();
        notify('La película se ha eliminado correctamente')->type('success');
        return redirect()->action('CatalogController@getIndex');
    }
}
