@extends('layouts.master')
 
@section('content')
    <div class="row">       
        <div class="col-sm-4">
            <img src="{{ $pelicula->poster }}" style="height:450px">     
        </div>      
        <div class="col-sm-8">           
            <h3 >{{ $pelicula->title }}</h3>
            <h5>Año: {{ $pelicula->year }}</h5>
            <h5>Director: {{ $pelicula->director }}</h5>
            <h6><b>Resumen: </b> {{ $pelicula->synopsis}}</h6>
            <br>
            <h6><b>Estado: </b>
                @if($pelicula->rented == TRUE)
                    Película actualmente alquilada <br>
                    <form action="{{action('CatalogController@putReturn', $pelicula->id)}}" method="POST" style="display:inline">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Devolver Pelicula</button>
                    </form>
                @else
                    Película disponible <br>
                    <form action="{{action('CatalogController@putRent', $pelicula->id)}}" method="POST" style="display:inline">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }} 
                        <button type="submit" class="btn btn-primary">Alquilar película</button>
                    </form>
                @endif
                <a class="btn btn-warning" href="{{ url('/catalog/edit/'.$pelicula->id) }}" role="button">
                    <i class="fas fa-pencil-alt"></i>
                    Editar película
                </a>
                <form action="{{action('CatalogController@deleteMovie', $pelicula->id)}}" method="POST" style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }} 
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-times"></i>
                        Eliminar película
                    </button>
                </form>
                <a class="btn btn-outline-dark" href="{{ url('/catalog/') }}" role="button">
                    <i class="fas fa-angle-left"></i>
                    Volver al listado
                </a>
                <br>
        </div> 
    </div>
@stop
